package com.devcamp.schoolclassroomapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.schoolclassroomapi.models.School;

@Service
public class SchoolService extends ClassroomService{
    School school1 = new School(1001, "Vinschool", "Q. Hai Bà Trưng, Hà Nội", getClassSchool1());
    School school2 = new School(1002, "Hanoi Amsterdam", "Q. Ba Đình, Hà Nội", getClassSchool2());
    School school3 = new School(1003, "ĐH KTQD", "P. Trần Đại Nghĩa, Hà Nội", getClassSchool3());
    public ArrayList<School> getAllSchools(){
        ArrayList<School> schoolList = new ArrayList<>();
        schoolList.add(school1);
        schoolList.add(school2);
        schoolList.add(school3);
        return schoolList;
    }
    public School getSchoolById(int id){
        School school = null;
        for (int i=0; i < getAllSchools().size(); i++) {
            if (getAllSchools().get(i).getId() == id){
                school = getAllSchools().get(i);
            }
        }
        return school;
    }
    public ArrayList<School> getSchoolsByStudentNumber(int noNumber){
        ArrayList<School> schools = new ArrayList<>();
        for (int i=0; i < getAllSchools().size(); i++){
            if (getAllSchools().get(i).getTotalStudent() > noNumber){
                schools.add(getAllSchools().get(i));
            }
        }
        return schools;
    }
     
}
