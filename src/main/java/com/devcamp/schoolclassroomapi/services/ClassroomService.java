package com.devcamp.schoolclassroomapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.schoolclassroomapi.models.Classroom;

@Service
public class ClassroomService {
    Classroom class1 = new Classroom(1, "Lớp 7A", 30);
    Classroom class2 = new Classroom(2, "Lớp 7B", 33);
    Classroom class3 = new Classroom(3, "Lớp 7C", 32);

    Classroom class4 = new Classroom(4, "Chuyên Toán", 35);
    Classroom class5 = new Classroom(5, "Chuyên Văn", 36);
    Classroom class6 = new Classroom(6, "Chuyên Anh", 37);

    Classroom class7 = new Classroom(7, "Kế toán", 74);
    Classroom class8 = new Classroom(8, "Kiểm toán", 68);
    Classroom class9 = new Classroom(9, "Tài chính doanh nghiệp", 70);

    public ArrayList<Classroom> getClassSchool1(){
        ArrayList<Classroom> classSchool1 = new ArrayList<>();
        classSchool1.add(class1);
        classSchool1.add(class2);
        classSchool1.add(class3);
        return classSchool1;
    }
    public ArrayList<Classroom> getClassSchool2(){
        ArrayList<Classroom> classSchool2 = new ArrayList<>();
        classSchool2.add(class4);
        classSchool2.add(class5);
        classSchool2.add(class6);
        return classSchool2;
    }
    public ArrayList<Classroom> getClassSchool3(){
        ArrayList<Classroom> classSchool3 = new ArrayList<>();
        classSchool3.add(class7);
        classSchool3.add(class8);
        classSchool3.add(class9);
        return classSchool3;
    }
    public ArrayList<Classroom> getAllClasses(){
        ArrayList<Classroom> classList = new ArrayList<>();
        classList.add(class1);
        classList.add(class2);
        classList.add(class3);
        classList.add(class4);
        classList.add(class5);
        classList.add(class6);
        classList.add(class7);
        classList.add(class8);
        classList.add(class9);
        return classList;
    }
    public ArrayList<Classroom> getClassByStudentNumber(int noNumber){
        ArrayList<Classroom> classes = new ArrayList<>();
        for (int i=0; i < getAllClasses().size(); i++){
            if (getAllClasses().get(i).getNoStudent() > noNumber){
                classes.add(getAllClasses().get(i));
            }
        }
        return classes;
    }
}
