package com.devcamp.schoolclassroomapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SchoolClassroomApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SchoolClassroomApiApplication.class, args);
	}

}
