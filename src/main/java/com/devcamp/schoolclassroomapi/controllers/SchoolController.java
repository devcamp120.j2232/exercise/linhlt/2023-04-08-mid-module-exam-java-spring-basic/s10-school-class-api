package com.devcamp.schoolclassroomapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.schoolclassroomapi.models.School;
import com.devcamp.schoolclassroomapi.services.SchoolService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class SchoolController {
    @Autowired
    SchoolService schoolService;
    @GetMapping("/schools")
    public ArrayList<School> getAllSchoolsApi(){
        return schoolService.getAllSchools();
    }
    @GetMapping("/schools/{schoolId}")
    public School getSchoolByIdApi(@PathVariable int schoolId){
        return schoolService.getSchoolById(schoolId);
    }
    @GetMapping("/schools-no-student-query")
    public ArrayList<School> getSchoolsByStudentNumberApi(@RequestParam (value="noNumber", defaultValue = "")int noNumber){
        return schoolService.getSchoolsByStudentNumber(noNumber);
    }
}
