package com.devcamp.schoolclassroomapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.schoolclassroomapi.models.Classroom;
import com.devcamp.schoolclassroomapi.services.ClassroomService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class ClassController {
    @Autowired
    ClassroomService classroomService;

    @GetMapping("/classes")
    public ArrayList<Classroom> getAllClassesApi(){
        return classroomService.getAllClasses();
    }
    @GetMapping("/classes-no-student-query")
    public ArrayList<Classroom> getClassesByStudentNumberApi(@RequestParam (value="noNumber", defaultValue = "")int noNumber){
        return classroomService.getClassByStudentNumber(noNumber);
    }
}
